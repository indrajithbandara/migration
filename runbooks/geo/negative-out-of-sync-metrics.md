# Negative out of sync metrics

## First and foremost

*Don't Panic*

## Symptoms

* The Geo status dashboard in Grafana shows negative numbers for the
Repositories or Wikis Out of Sync:

    ![Repos and wikis Out of Sync negative](../img/geo-negative-out-of-sync.png)

## Diagnose projects

* Get list of project IDs that should not exist in the tracking database:a

    ```ruby
    project_ids = Project.pluck(:id) ; project_ids.count
    registry_projects = Geo::ProjectRegistry.pluck(:project_id) ; registry_projects.count
    should_not_be_present = (registry_projects - project_ids) ; should_not_be_present.count
    ```

* Count how many of those are failures:

    ```ruby
    Geo::ProjectRegistry.where(project_id: should_not_be_present).failed.count
    Geo::ProjectRegistry.where(project_id: should_not_be_present).failed_repos.count
    Geo::ProjectRegistry.where(project_id: should_not_be_present).failed_wikis.count
    ```

* Find associated audit events:

    ```ruby
    AuditEvent.where(entity_type: 'Project', entity_id: should_not_be_present).where("details LIKE '%remove%project%'").count
    ```

* Find associated Geo Deleted Events:

    ```ruby
    Geo::RepositoryDeletedEvent.where(project_id: should_not_be_present).count
    ```

    Keep in mind, the Geo log events might be pruned.

## Run reconciliation

* Get the list of orphaned projects

    ```ruby
    project_ids = Project.pluck(:id) ; project_ids.count
    registry_projects = Geo::ProjectRegistry.pluck(:project_id) ; registry_projects.count
    should_not_be_present = (registry_projects - project_ids) ; should_not_be_present.count
    should_not_be_present
    ```

* Remove project_registry rows that should not be present

    ```ruby
    Geo::ProjectRegistry.where(project_id: should_not_be_present).count
    Geo::ProjectRegistry.where(project_id: should_not_be_present).delete_all
    ```

* TODO: How to delete the projects from disk


## Post diagnose

* Check the status again:

    ```ruby
    pp GeoNodeStatus.new(geo_node: Gitlab::Geo.current_node).load_data_from_current_node
    ```

## Related issues

gitlab-com/migration#295
